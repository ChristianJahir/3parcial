class Arbol {

    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        //this.nivel = 4;
        this.busquedaElemento;
        //this.buscarNodos = [];
        this.sumarCamino = 0 ;
        this.caminoNodo = '';
    }

    agregarNodoPadre(){
        var nodo = new Nodo(null,0,null,0,0);
        return nodo;
    }

    agregarNodo(nodoPadre,nivel,posicion,elementos,numeros){
        let nodo = new Nodo(nodoPadre,nivel,posicion,elementos,numeros);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }
    buscarValor(buscarElemento,nodo){

        if(nodo.nombre == buscarElemento && this.busquedaElemento == null)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        return this.busquedaElemento;
    }

    buscarCaminoNodo(nodo){
        if(nodo.padre != null){
            this.caminoNodo = this.caminoNodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }

        return this.busquedaElemento.nombre+' '+this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if (nodo.padre != null) {
            this.sumarCamino = this.sumarCamino + nodo.padre.nombre;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre + this.sumarCamino;
    }
}